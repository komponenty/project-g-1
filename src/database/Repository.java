/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

class Repository {
    
    public Repository(){
    }
    
    public SessionFactory getSession(){
         SessionFactory sf = //HibernateUtil.getSessionFactory();
                new AnnotationConfiguration().
                   configure().
                   addAnnotatedClass(Customer.class).
                 addAnnotatedClass(Meeting.class).
                 addAnnotatedClass(Employee.class).
                 addAnnotatedClass(Task.class).
                   buildSessionFactory();
         return sf;
    }
}
