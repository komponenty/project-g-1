/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Meeting {
    @Id
    @GeneratedValue
    private Integer id;
    private String title;
    private String meetingStart;
    private String meetingEnd;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Customer> participants;
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the meetingStart
     */
    public String getMeetingStart() {
        return meetingStart;
    }

    /**
     * @param meetingStart the meetingStart to set
     */
    public void setMeetingStart(String meetingStart) {
        this.meetingStart = meetingStart;
    }

    /**
     * @return the meetingEnd
     */
    public String getMeetingEnd() {
        return meetingEnd;
    }

    /**
     * @param meetingEnd the meetingEnd to set
     */
    public void setMeetingEnd(String meetingEnd) {
        this.meetingEnd = meetingEnd;
    }

    /**
     * @return the participants
     */
    public Set<Customer> getParticipants() {
        return participants;
    }

    /**
     * @param participants the participants to set
     */
    public void setParticipants(Set<Customer> participants) {
        this.participants = participants;
    }
}
