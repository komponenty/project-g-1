/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

@Service
public class EmployeeRepository extends Repository{
    
     public List<Employee> getEmployee(){
        Session session = getSession().openSession();
        Criteria criteria = session.createCriteria(Employee.class);
        List<Employee> result =  criteria.list();
        session.close();
        return result;
    }
    
    public void addEmployee(Employee employee){
        Session session = getSession().openSession();
        
        Transaction tx = session.beginTransaction();
        session.save(employee);
        
        tx.commit();
    }
    
    public void updateEmployee(Employee employee){
        Session session = getSession().openSession();
        
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(employee);
        
        tx.commit();
    }
}
