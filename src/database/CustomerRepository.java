/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;


@Service
public class CustomerRepository extends Repository{
    
    public CustomerRepository(){
        super();
    }
    
    public void addCustomer(Customer c){
       
        Session session = getSession().openSession();
        Transaction txn = session.beginTransaction();
        session.save(c);
        txn.commit();
        session.close();
    }
    
     public void updateCustomer(Customer c){
       Session session = getSession().openSession();
        Transaction txn = session.beginTransaction();
        session.saveOrUpdate(c);
        txn.commit();
        session.close();
    }
    
    public List<Customer> getCustomers(){
        Session session = getSession().openSession();
        Criteria criteria = session.createCriteria(Customer.class);
        List<Customer> result =  criteria.list();
        session.close();
        return result;
    }
    public static void main(String[] args){
        Customer c = new Customer();
        c.setAddress("aadresss");
        new CustomerRepository().addCustomer(c);
    }
}
