/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;


@org.springframework.stereotype.Repository
public class TaskRepository extends Repository{
    public void addTask(Task task){
         Session session = getSession().openSession();
        Transaction txn = session.beginTransaction();
        session.save(task);
        txn.commit();
        session.close(); 
    }
    public void remove(Task task){
        Session session = getSession().openSession();
        Transaction txn = session.beginTransaction();
        session.delete(task);
        txn.commit();
        session.close(); 
    }
    
     public List<Task> getTasks(){
        Session session = getSession().openSession();
        Criteria criteria = session.createCriteria(Task.class);
        List<Task> result =  criteria.list();
        session.close();
        return result;
    }
}
