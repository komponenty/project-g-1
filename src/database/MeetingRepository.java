/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import org.hibernate.Session;
import org.hibernate.Transaction;


@org.springframework.stereotype.Repository
public class MeetingRepository extends Repository{
    
    public void addMeeting(Meeting m){
        Session session = getSession().openSession();
        Transaction txn = session.beginTransaction();
        session.save(m);
        txn.commit();
        session.close();
    }
}
