package xmlGenerator;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.springframework.stereotype.Service;

@Service
public class XMLGenerator {

	
	public void save(XMLDocument document) throws Exception{
		File file = new File("Raport.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(XMLDocument.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(document, file);
		jaxbMarshaller.marshal(document, System.out);
	}
	
	
	
	public static void main(String[] args){
		
		
		XMLDocument doc = new XMLDocument(); 
		
		XMLPosition pos1 = new XMLPosition();
		XMLPosition pos2 = new XMLPosition();
		
		
		XMLElement el1 = new XMLElement();
		el1.setName("id1");
		el1.setValue(123+"");
		
		XMLElement el2 = new XMLElement();
		el2.setName("id2");
		el2.setValue(123+"");
		
		XMLElement el3 = new XMLElement();
		el3.setName("id3");
		el3.setValue(123+"");
		
		pos1.getElementList().add(el1);
		pos1.getElementList().add(el2);
		pos1.getElementList().add(el3);
		
		pos2.getElementList().add(el1);
		pos2.getElementList().add(el2);
		
		doc.getPositions().add(pos1);
		doc.getPositions().add(pos2);
		
		try {
			new XMLGenerator().save(doc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
