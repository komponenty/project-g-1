package xmlGenerator;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "elements")
public class XMLPosition {
	
	public XMLPosition(){
		elementList = new ArrayList<XMLElement>();
	}

	  @XmlElement(name = "xmlElement", type = XMLElement.class)
	private List<XMLElement> elementList;

	public List<XMLElement> getElementList() {
		return elementList;
	}

	public void setElementList(List<XMLElement> elementList) {
		this.elementList = elementList;
	}
}
