package xmlGenerator;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="document")
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLDocument {
	
	public XMLDocument(){
		positions = new ArrayList<XMLPosition>();
	}
	
	@XmlElement(name = "elements", type = XMLPosition.class)
	private List<XMLPosition> positions;

	public List<XMLPosition> getPositions() {
		return positions;
	}

	public void setPositions(List<XMLPosition> positions) {
		this.positions = positions;
	}
	
	
	
}
