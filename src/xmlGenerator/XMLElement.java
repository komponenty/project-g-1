package xmlGenerator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class XMLElement {
	private String name;
	private String value;
	public String getName() {
		return name;
	}
	
	@XmlElement
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	
	@XmlElement
	public void setValue(String value) {
		this.value = value;
	}
}
